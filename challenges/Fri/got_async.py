#!/usr/bin/python3
import asyncio
import time
import aiohttp

async def get_character(session, char_num):
    async with session.get(f"https://anapioficeandfire.com/api/characters/{char_num}") as resp:
        result = await resp.json()
        print(str(char_num), result["name"])


async def main():
    async with aiohttp.ClientSession() as session:
        tasks = [asyncio.create_task(get_character(session, char_num)) for char_num in range(1,101)]
        await asyncio.gather(*tasks)


if __name__ == "__main__":
    start = time.time()
    asyncio.run(main())
    print(f'Time taken: {time.time() - start}')
