#!/usr/bin/env python3
from netmiko import ConnectHandler
import json
from pprint import pprint

# device list
SW1 = ["arista_eos", "sw-1", "admin", "alta3"]
SW2 = ["arista_eos", "sw-2", "admin", "alta3"]
NXOS = ["cisco_nxos", "sandbox-nxos-1.cisco.com", "admin", "Admin_1234!"]

devices = [SW1, SW2, NXOS]

results = {}

with open("switch_cmds.txt", "r") as f:
    commands = f.read().splitlines()

# run commands
def run_commands(device):
    results[device[1]] = {}
    open_connection = ConnectHandler(device_type=device[0], ip=device[1], username=device[2], password=device[3])
    for command in commands:
        my_command = open_connection.send_command(command)
        results[device[1]][command] = my_command
        print(f"*** Running '{command}' on {device[1]} ***")
        print(my_command)
        print("\n\n")
    print("=" * 50, "\n\n")

## main routine
if __name__ == "__main__":
    for device in devices:
        run_commands(device)

    with open("results.json", "w") as r:
        r.write(json.dumps(results))
    
    pprint(results)
